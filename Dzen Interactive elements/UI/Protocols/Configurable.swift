//
//  Configurable.swift
//  Dzen Interactive elements
//
//  Created by r.khomenko on 06.01.2023.
//

public protocol Configurable {
	associatedtype ViewModel
	
	func setup(with viewModel: ViewModel)
}
