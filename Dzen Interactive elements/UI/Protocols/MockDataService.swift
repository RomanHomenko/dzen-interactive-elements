//
//  MockDataService.swift
//  Dzen Interactive elements
//
//  Created by r.khomenko on 06.01.2023.
//

import Foundation

public enum RequestType {
	case GET
	case POST
}

public protocol MockDataService {
	func sendRequest(type: RequestType)
}
