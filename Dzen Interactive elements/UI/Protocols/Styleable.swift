//
//  Styleable.swift
//  Dzen Interactive elements
//
//  Created by r.khomenko on 06.01.2023.
//

public protocol Styleable {
	associatedtype Style
	func applyStyle(_ style: Style)
}
