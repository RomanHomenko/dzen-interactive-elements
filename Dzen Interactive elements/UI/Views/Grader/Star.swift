//
//  Star.swift
//  Dzen Interactive elements
//
//  Created by r.khomenko on 04.01.2023.
//

import UIKit
import Foundation

final class Star: UIView {
	private var cornerRadius: CGFloat = 0
	private var fillColor: UIColor = .clear
	public var isSelected: Bool = false
	
	public init() {
		super.init(frame: .zero)
		backgroundColor = .clear
	}
	
	required init?(coder: NSCoder) {
		super.init(coder: coder)
		backgroundColor = .clear
	}
	
	override func draw(_ rect: CGRect) {
		let numberOfPoints: CGFloat = 5

		let starRatio: CGFloat = 0.5

		let steps: CGFloat = numberOfPoints * 2

		let outerRadius: CGFloat = min(rect.height, rect.width) / 2
		let innerRadius: CGFloat = outerRadius * starRatio

		let stepAngle = CGFloat(2) * CGFloat(Double.pi) / CGFloat(steps)
		let center = CGPoint(x: rect.width / 2, y: rect.height / 2)

		let path = UIBezierPath()

		for i in 0..<Int(steps) {
			let radius = i % 2 == 0 ? outerRadius : innerRadius

			let angle = CGFloat(i) * stepAngle - CGFloat(Double.pi / 2)

			let x = radius * cos(angle) + center.x
			let y = radius * sin(angle) + center.y

			if i == 0 {
				path.move(to: CGPoint(x: x, y: y))
			}
			else {
				path.addLine(to: CGPoint(x: x, y: y))
			}
		}

		path.close()
		isSelected ? fillColor.setFill() : fillColor.setStroke()
		isSelected ? path.fill() : path.stroke()
	}
}

extension Star: Styleable {
	public struct Style {
		public let color: UIColor
		public let cornerRadius: CGFloat
		
		public init(color: UIColor, cornerRadius: CGFloat) {
			self.color = color
			self.cornerRadius = cornerRadius
		}
	}
	
	public func applyStyle(_ style: Style) {
		cornerRadius = style.cornerRadius
		fillColor = style.color
	}
}
