//
//  GraderView.swift
//  Dzen Interactive elements
//
//  Created by r.khomenko on 04.01.2023.
//

import UIKit

final class GraderView: UIStackView {
	private(set) var action: ((Int) -> Void)?
	
	public init() {
		super.init(frame: .zero)
		setupViews()
	}
	
	required init(coder: NSCoder) {
		super.init(coder: coder)
		setupViews()
	}
	
	private func setupViews(oldRating: Int = -1) {
		arrangedSubviews.forEach({ $0.removeFromSuperview() })
		distribution = .fillEqually
		axis = .horizontal
		for index in 0..<5 {
			let star = Star()
			if (oldRating - 1) >= index {
				star.isSelected = true
			}
			star.tag = index
			addArrangedSubview(star)
		}
	}

	
	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		super.touchesBegan(touches, with: event)
		guard let touch = touches.first else { return }
		guard let touchedView = hitTest(touch.location(in: self), with: nil) else { return }
		action?(touchedView.tag + 1)
		
		for (index, subview) in arrangedSubviews.enumerated() {
			if let star = subview as? Star {
				star.isSelected = index <= touchedView.tag
				star.setNeedsDisplay()
			}
		}
	}
}

extension GraderView: Styleable {
	public struct Style {
		public let starStyle: Star.Style
		public let backgroundColor: UIColor
		
		public init(starStyle: Star.Style, backgroundColor: UIColor = .clear) {
			self.starStyle = starStyle
			self.backgroundColor = backgroundColor
		}
	}
	
	public func applyStyle(_ style: Style) {
		backgroundColor = style.backgroundColor
		for subview in arrangedSubviews {
			if let star = subview as? Star {
				star.applyStyle(style.starStyle)
			}
		}
	}
}

extension GraderView: Configurable {
	public struct ViewModel {
		public let rating: Int
		public let action: ((Int) -> Void)? /// for saving after choose rating
		
		public init(rating: Int = -1, action: ((Int) -> Void)? = nil) {
			self.rating = rating
			self.action = action
		}
	}
	
	public func setup(with viewModel: ViewModel) {
		action = viewModel.action
		setupViews(oldRating: viewModel.rating)
	}
}
