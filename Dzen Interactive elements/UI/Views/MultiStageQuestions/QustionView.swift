//
//  QustionView.swift
//  Dzen Interactive elements
//
//  Created by r.khomenko on 04.01.2023.
//

import UIKit

final public class QuestionView: UIView {
	private var numeration = UILabel()
	private var question = UILabel()
	private var answers = UIStackView()
	
	private var viewModel: ViewModel?
	
	private var tapCount: Int = 0
	private var action: ((Int) -> Void)?
	
	public init() {
		super.init(frame: .zero)
		setupViwes()
		setupConstraints()
	}
	
	required init?(coder: NSCoder) {
		super.init(coder: coder)
		setupViwes()
		setupConstraints()
	}
	
	public func updateAnswers() {}
	
	private func setupViwes() {
		numeration.translatesAutoresizingMaskIntoConstraints = false
		question.translatesAutoresizingMaskIntoConstraints = false
		answers.translatesAutoresizingMaskIntoConstraints = false
		
		addSubview(numeration)
		addSubview(question)
		addSubview(answers)
		
		answers.axis = .vertical
		answers.distribution = .fillEqually
	}
	
	private func setupConstraints() {
		NSLayoutConstraint.activate([
			numeration.topAnchor.constraint(equalTo: self.topAnchor, constant: 32),
			numeration.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 16),
			numeration.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -16),
		])
		
		NSLayoutConstraint.activate([
			question.topAnchor.constraint(equalTo: numeration.bottomAnchor, constant: 32),
			question.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 16),
			question.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -16),
		])
		
		NSLayoutConstraint.activate([
			answers.topAnchor.constraint(equalTo: question.bottomAnchor, constant: 16),
			answers.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 16),
			answers.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -16),
			answers.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -16),
		])
	}
	
	private func setupAnswers(answers: [AnswerSection.ViewModel], checkmark: UIImage) {
		for answerVM in answers {
			var updatedAnswerVM = answerVM
			updatedAnswerVM.checkmark = checkmark
			let answer = AnswerSection()
			answer.accessibilityIdentifier = "answer_\(answerVM.text)_acessibility"
			answer.setup(with: updatedAnswerVM)
			self.answers.addArrangedSubview(answer)
		}
	}
}

extension QuestionView {
	public override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		isUserInteractionEnabled = false
		tapCount += 1
		super.touchesBegan(touches, with: event)
	}
	
	public override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
		for answer in answers.arrangedSubviews {
			if let answer = answer as? AnswerSection {
				answer.updatePercent(allTappedAnswers: tapCount)
				answer.showPercent()
			}
		}
		action?(tapCount)
		super.touchesEnded(touches, with: event)
	}
}

extension QuestionView: Styleable {
	public struct Style {
		let answerSectionStyle: AnswerSection.Style
		
		public init(answerSectionStyle: AnswerSection.Style) {
			self.answerSectionStyle = answerSectionStyle
		}
	}
	
	public func applyStyle(_ style: Style) {
		for answer in answers.subviews {
			if let answer = answer as? AnswerSection {
				answer.applyStyle(style.answerSectionStyle)
			}
		}
	}
}

extension QuestionView: Configurable {
	public struct ViewModel {
		public let title: String
		public let questionIndex: Int
		public let allQustionsCount: Int
		public let answers: [AnswerSection.ViewModel]
		public var checkmark: UIImage = UIImage()
		public let action: ((Int) -> Void)?
		
		public init(title: String,
					questionIndex: Int,
					allQustionsCount: Int,
					answers: [AnswerSection.ViewModel],
					action: ((Int) -> Void)? = nil) {
			self.title = title
			self.answers = answers
			self.questionIndex = questionIndex
			self.allQustionsCount = allQustionsCount
			self.action = action
		}
	}
	
	public func setup(with viewModel: ViewModel) {
		self.viewModel = viewModel
		let count = viewModel.allQustionsCount
		let index = viewModel.questionIndex
		accessibilityIdentifier = "\(viewModel.title)_acessibilityId"
		numeration.text = "Qustion \(index+1)/\(count)"
		question.text = viewModel.title
		for answer in viewModel.answers {
			tapCount += answer.tapCount
		}
		action = viewModel.action
		
		setupAnswers(answers: viewModel.answers, checkmark: viewModel.checkmark)
	}
}
