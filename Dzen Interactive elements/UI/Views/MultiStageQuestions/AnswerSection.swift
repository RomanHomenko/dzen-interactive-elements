//
//  AnswerSection.swift
//  Dzen Interactive elements
//
//  Created by r.khomenko on 04.01.2023.
//

import UIKit

final public class AnswerSection: UIView {
	public var allTappedCount: Int = 0
	
	private var tapCount: Int = 0
	private var action: ((Int) -> Void)?
	
	private let title = UILabel()
	private let percent = UILabel()
	private let checkmarkImage = UIImageView()
	
	private var style: Style?
	
	private var isCorrect: Bool = false
	private var isSelected: Bool = false
	
	public init() {
		super.init(frame: .zero)
		setupViwes()
		setupConstraints()
	}
	
	required init?(coder: NSCoder) {
		super.init(coder: coder)
		setupViwes()
		setupConstraints()
	}
	
	public func showPercent() {
		percent.isHidden = false
	}
		
	private func setupViwes() {
		title.translatesAutoresizingMaskIntoConstraints = false
		percent.translatesAutoresizingMaskIntoConstraints = false
		checkmarkImage.translatesAutoresizingMaskIntoConstraints = false
		
		addSubview(title)
		addSubview(percent)
		addSubview(checkmarkImage)
	}
	
	private func setupConstraints() {
		NSLayoutConstraint.activate([
			title.centerYAnchor.constraint(equalTo: self.centerYAnchor),
			title.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 16),
			title.rightAnchor.constraint(equalTo: checkmarkImage.leftAnchor, constant: 16),
		])
		
		NSLayoutConstraint.activate([
			percent.centerYAnchor.constraint(equalTo: centerYAnchor),
			percent.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -16),
		])
		
		NSLayoutConstraint.activate([
			checkmarkImage.centerYAnchor.constraint(equalTo: self.centerYAnchor),
			checkmarkImage.rightAnchor.constraint(equalTo: percent.leftAnchor),
		])
		checkmarkImage.setContentHuggingPriority(.defaultLow, for: .horizontal)
		percent.setContentHuggingPriority(.defaultHigh, for: .horizontal)
	}
	
	private func changeState() {
		guard let style = style else { return }
		backgroundColor = isCorrect ? style.correctColor : style.incorrectColor
		checkmarkImage.isHidden = !isCorrect
		isSelected = true
	}
	
	public func updatePercent(allTappedAnswers: Int) {
		let percents = Double(tapCount)/Double(allTappedAnswers) * 100.0
		let roundedPercents = Double(String(format: "%.2f", percents))
		percent.text = "\(roundedPercents ?? 0.0)%"
	}
}

extension AnswerSection: Styleable {
	public struct Style {
		public let backgroundColor: UIColor
		public let correctColor: UIColor
		public let incorrectColor: UIColor
		
		public init(backgroundColor: UIColor,
					correctColor: UIColor,
					incorrectColor: UIColor) {
			self.backgroundColor = backgroundColor
			self.correctColor = correctColor
			self.incorrectColor = incorrectColor
		}
	}
	
	public func applyStyle(_ style: Style) {
		self.style = style
		backgroundColor = style.backgroundColor
	}
}

extension AnswerSection: Configurable {
	public struct ViewModel {
		public let text: String
		public let isCorrect: Bool
		public let height: Int
		public var tapCount: Int
		public var checkmark: UIImage = UIImage()
		public let action: ((Int) -> Void)?
		
		public init(text: String,
					isCorrect: Bool,
					tapCount: Int = 0,
					height: Int = 70,
					action: ((Int) -> Void)? = nil) {
			self.text = text
			self.isCorrect = isCorrect
			self.tapCount = tapCount
			self.height = height
			self.action = action
		}
	}
	
	public func setup(with viewModel: ViewModel) {
		isCorrect = viewModel.isCorrect
		title.text = viewModel.text
		percent.text = "0%"
		percent.isHidden = true
		tapCount = viewModel.tapCount
		action = viewModel.action
		checkmarkImage.image = viewModel.checkmark
		checkmarkImage.isHidden = true
		
		heightAnchor.constraint(equalToConstant: CGFloat(viewModel.height)).isActive = true
	}
}

extension AnswerSection {
	public override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
		changeState()
		tapCount += 1
		updatePercent(allTappedAnswers: allTappedCount)
		action?(tapCount)
		super.touchesEnded(touches, with: event)
	}
}
