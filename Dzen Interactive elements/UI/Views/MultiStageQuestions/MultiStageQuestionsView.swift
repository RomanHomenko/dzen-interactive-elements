//
//  MultiStageQuestionsView.swift
//  Dzen Interactive elements
//
//  Created by r.khomenko on 04.01.2023.
//

import UIKit

final public class MultiStageQuestions: UIStackView {
	private var style: Style?
	
	public init() {
		super.init(frame: .zero)
		setupView()
	}
	
	required init(coder: NSCoder) {
		super.init(coder: coder)
		setupView()
	}
	
	private func setupView() {
		self.axis = .vertical
		self.distribution = .fillEqually
	}
}

extension MultiStageQuestions: Styleable {
	public struct Style {
		public let questionStyle: QuestionView.Style
		
		public init(questionStyle: QuestionView.Style) {
			self.questionStyle = questionStyle
		}
	}
	
	public func applyStyle(_ style: Style) {
		for arrangedQuestion in arrangedSubviews {
			if let question = arrangedQuestion as? QuestionView {
				question.applyStyle(style.questionStyle)
			}
		}
	}
}

extension MultiStageQuestions: Configurable {
	public struct ViewModel {
		public let questions: [QuestionView.ViewModel]
		public let checkmark: UIImage
		
		public init(questions: [QuestionView.ViewModel], checkmark: UIImage) {
			self.questions = questions
			self.checkmark = checkmark
		}
	}
	
	public func setup(with viewModel: ViewModel) {
		for question in viewModel.questions {
			accessibilityIdentifier = "MultiStageQuestions_acessibilityId"
			let questionView = QuestionView()
			var updatedQuestionVM = question
			updatedQuestionVM.checkmark = viewModel.checkmark
			questionView.setup(with: updatedQuestionVM)
			addArrangedSubview(questionView)
		}
	}
}
