//
//  MultiStageQuestionsView+Style.swift
//  Dzen Interactive elements
//
//  Created by r.khomenko on 06.01.2023.
//

import UIKit

public extension MultiStageQuestions.Style {
	static let dzenStyle = MultiStageQuestions.Style(
		questionStyle: QuestionView.Style(
			answerSectionStyle: AnswerSection.Style(
				backgroundColor: .gray,
				correctColor: .green.withAlphaComponent(0.9),
				incorrectColor: .darkGray.withAlphaComponent(0.9)
			)
		)
	)
}
