//
//  MultiStageQuestionsViewController.swift
//  Dzen Interactive elements
//
//  Created by r.khomenko on 04.01.2023.
//

import UIKit

class MultiStageQuestionsViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()

		view.backgroundColor = .white

		let multiStageQuestions = MultiStageQuestions()
		let answers1VM = AnswerSection.ViewModel(text: "1", isCorrect: false, tapCount: 4) { tapCount in
			print("saved for first answer taps: \(tapCount)")
		}
		let answers2VM = AnswerSection.ViewModel(text: "2", isCorrect: true, tapCount: 4) { tapCount in
			print("saved for second answer taps: \(tapCount)")
		}
		let answers3VM = AnswerSection.ViewModel(text: "3", isCorrect: false, tapCount: 2) { tapCount in
			print("saved for third answer taps: \(tapCount)")
		}
		let questionVM = QuestionView.ViewModel(title: "First question",
												questionIndex: 0,
												allQustionsCount: 1,
												answers: [answers1VM, answers2VM, answers3VM]) { tapCount in
			print("saved allTapCount: \(tapCount)")
		} /// set data from remote repository
		let vm = MultiStageQuestions.ViewModel(questions: [questionVM], checkmark: .checkmark)
		multiStageQuestions.setup(with: vm)
		multiStageQuestions.applyStyle(.dzenStyle)

		view.addSubview(multiStageQuestions)
		multiStageQuestions.translatesAutoresizingMaskIntoConstraints = false
		NSLayoutConstraint.activate([
			multiStageQuestions.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 16),
			multiStageQuestions.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -16),
			multiStageQuestions.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 16),
		])
    }
}
