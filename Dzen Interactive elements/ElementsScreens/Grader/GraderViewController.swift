//
//  GraderViewController.swift
//  Dzen Interactive elements
//
//  Created by r.khomenko on 04.01.2023.
//

import UIKit

final class GraderViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

		view.backgroundColor = .white

		let grader = GraderView()
		let vm = GraderView.ViewModel(rating: 3, action: { rating in
			print("tapped rating: \(rating)")
		})
		let starStyle = Star.Style(color: .red, cornerRadius: 10)
		let style = GraderView.Style(starStyle: starStyle)
		grader.setup(with: vm)
		grader.applyStyle(style)
		view.addSubview(grader)

		grader.translatesAutoresizingMaskIntoConstraints = false
		NSLayoutConstraint.activate([
			grader.centerXAnchor.constraint(equalTo: view.centerXAnchor),
			grader.centerYAnchor.constraint(equalTo: view.centerYAnchor),
			grader.widthAnchor.constraint(equalToConstant: view.frame.width),
			grader.heightAnchor.constraint(equalToConstant: 50),
		])
    }
}
