//
//  StarterTableViewController.swift
//  Dzen Interactive elements
//
//  Created by r.khomenko on 04.01.2023.
//

import UIKit

final class StarterTableViewController: UITableViewController {
	private struct Constants {
		static let cellId = "Cell"
		static let rowHeight: CGFloat = 44
	}

	private let sections: [Sections] = Sections.allCases

	override func viewDidLoad() {
		super.viewDidLoad()

		title = "Interactive elements"
		
		tableView.register(UITableViewCell.self, forCellReuseIdentifier: Constants.cellId)
		tableView.rowHeight = Constants.rowHeight
		tableView.estimatedRowHeight = Constants.rowHeight
	}

	override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return sections.count
	}

	override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		guard let cell = tableView.dequeueReusableCell(withIdentifier: Constants.cellId) else { return UITableViewCell() }
		let title = sections[indexPath.row].stringValue
		cell.textLabel?.text = title
		return cell
	}

	override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		tableView.deselectRow(at: indexPath, animated: true)
		let section = sections[indexPath.row]
		var vc = UIViewController()
		switch section {
		case .multiStageQuestionsView:
			vc = MultiStageQuestionsViewController()
		case .matcherView: break
			
		case .dragWordsView: break
			
		case .emptyTextFieldView: break
			
		case .graderView:
			vc = GraderViewController()
		}
		navigationController?.pushViewController(vc, animated: true)
	}
}

private enum Sections: Int, CaseIterable {
	case multiStageQuestionsView
	case matcherView
	case dragWordsView
	case emptyTextFieldView
	case graderView

	var stringValue: String {
		switch self {
		case .multiStageQuestionsView: return "MultiQuestions"
		case .matcherView: return "Matcher"
		case .dragWordsView: return "DragWords"
		case .emptyTextFieldView: return "EmptyTextField"
		case .graderView: return "Grader"
		}
	}
}
